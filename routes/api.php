<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

 use App\Http\Controllers\API\RegisterController;
 use App\Http\Controllers\API\NoteController;
 use App\Http\Controllers\API\ColorController;
//  use App\Http\Controllers\UserController;
//  use App\Http\Controllers\NoteController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::controller(RegisterController::class)->group(function(){
    Route::post('register', 'register');
    Route::post('login', 'login');
});
// Route::controller(NoteController::class)
//     ->group(function(){
//         Route::post('notes', 'store');
//         Route::get('notes', 'index');
//         Route::get('notes/{id}', 'show(id)');
// });
// Route::controller(colorController::class)
//     ->group(function(){
//         Route::post('colors', 'store');
//         Route::get('colors', 'show');
// });
// Route::controller('notes', NoteController::class);
//     Route::resource('colors', ColorController::class);


Route::middleware('auth:sanctum')->group( function () {
    Route::resource('notes', NoteController::class);
    Route::resource('colors', ColorController::class);
});

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
